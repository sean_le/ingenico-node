"use strict";
/*
 * This file was auto-generated from the API references found at
 * https://developer.globalcollect.com/documentation/api/server/
 */
var modules = {};

modules['cards'] = require('./cards');
// console.log(`Added cards to module space`);
modules['bankaccounts'] = require('./bankaccounts');
// console.log(`Added bankaccounts to module space`);

module.exports = modules;
