"use strict";
/*
 * This file was auto-generated from the API references found at
 * https://developer.globalcollect.com/documentation/api/server/
 */
var modules = {};

modules['create'] = require('./create');
// console.log(`Added create to module space`);
modules['get'] = require('./get');
// console.log(`Added get to module space`);

module.exports = modules;
