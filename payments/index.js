"use strict";
/*
 * This file was auto-generated from the API references found at
 * https://developer.globalcollect.com/documentation/api/server/
 */
var modules = {};

modules['refund'] = require('./refund');
// console.log(`Added refund to module space`);
modules['processchallenged'] = require('./processchallenged');
// console.log(`Added processchallenged to module space`);
modules['get'] = require('./get');
// console.log(`Added get to module space`);
modules['create'] = require('./create');
// console.log(`Added create to module space`);
modules['tokenize'] = require('./tokenize');
// console.log(`Added tokenize to module space`);
modules['cancel'] = require('./cancel');
// console.log(`Added cancel to module space`);
modules['approve'] = require('./approve');
// console.log(`Added approve to module space`);
modules['cancelapproval'] = require('./cancelapproval');
// console.log(`Added cancelapproval to module space`);

module.exports = modules;
