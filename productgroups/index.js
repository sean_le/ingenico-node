"use strict";
/*
 * This file was auto-generated from the API references found at
 * https://developer.globalcollect.com/documentation/api/server/
 */
var modules = {};

modules['find'] = require('./find');
// console.log(`Added find to module space`);
modules['get'] = require('./get');
// console.log(`Added get to module space`);

module.exports = modules;
